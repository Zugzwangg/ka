/*
 * Tez Tour core library.
 * Copyright(c) 2013-2018. 
 * April 15, 2016.
 * tez-tour 
 * 
 */
package com.cleverforms.iss.server.model.hotel;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Any;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.MetaValue;

import com.cleverforms.comms.server.model.Entity;
import com.cleverforms.comms.server.model.PersistentEntity;
import com.cleverforms.comms.server.provider.BasePersistentProvider;
import com.cleverforms.comms.server.util.Util;
import com.cleverforms.comms.shared.proxy.BaseProxy;
import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;
import com.cleverforms.ics.db.model.dictionary.City;
import com.cleverforms.ics.db.model.dictionary.Country;
import com.cleverforms.ics.db.model.dictionary.Region;
import com.cleverforms.ics.db.model.pub.ExtSystem;
import com.cleverforms.iss.server.model.airline.Airport;
import com.cleverforms.iss.server.model.hotel.Hotel;
import com.cleverforms.iss.server.model.hotel.HotelClass;
import com.cleverforms.iss.server.model.hotel.HotelFood;
import com.cleverforms.iss.server.model.hotel.RoomCategory;
import com.cleverforms.iss.shared.model.ExternalSystemAliasModel;
import com.cleverforms.iss.shared.proxy.ExternalSystemAliasProxy;

/**
 * A base entity for food in hotel dictionary data.
 * @author Sergey Titarchuk
 */
@javax.persistence.Entity
@Table(schema = "hotel", name = "ext_system_alias")
@AnyMetaDef(name = "objectMetaDef", idType = "long", metaType = "string", metaValues = {
	@MetaValue( value = "COUNTRY", targetEntity = Country.class ),
	@MetaValue( value = "REGION", targetEntity = Region.class ),
	@MetaValue( value = "CITY", targetEntity = City.class ),
	@MetaValue( value = "AIRPORT", targetEntity = Airport.class ),
	@MetaValue( value = "HOTEL", targetEntity = Hotel.class ),
	@MetaValue( value = "HOTEL_FOOD", targetEntity = HotelFood.class ),
	@MetaValue( value = "HOTEL_CLASS", targetEntity = HotelClass.class ),
	@MetaValue( value = "ROOM_CATEGORY", targetEntity = RoomCategory.class )
})
public class ExternalSystemAlias extends PersistentEntity {
	
	/** The serial version UID */
	private static final long serialVersionUID = 7510277175698750748L;

	private ExtSystem externalSystem;
	private String alias;
	private Entity object;

	@OneToOne
	@JoinColumn(name = "external_system_id")
	public ExtSystem getExternalSystem() {
		return externalSystem;
	}

	public void setExternalSystem(ExtSystem externalSystem) {
		this.externalSystem = externalSystem;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	@JoinColumn( name = "object_id" )
	@Any(metaDef = "objectMetaDef", metaColumn = @Column( name = "object_type" ))
	public Entity getObject() {
		return object;
	}

	public void setObject(Entity entity) {
		this.object = entity;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ExternalSystemAlias) {
			final ExternalSystemAlias entity = (ExternalSystemAlias) obj;
			return super.equals(obj) 
				&& Util.equalWithNull(externalSystem, entity.getExternalSystem())
				&& Util.equalWithNull(this.object, entity.getObject())
				&& Util.equalWithNull(alias, entity.getAlias());
		}
		return false;
	}

	@Override
	protected ExternalSystemAliasProxy proxyInstance() {
		return new ExternalSystemAliasModel(getId());
	}

	@Override
	public ExternalSystemAliasProxy getProxy(Object... params) {
		final ExternalSystemAliasProxy proxy = (ExternalSystemAliasProxy) super.getProxy(params);
		if (externalSystem != null) proxy.setExternalSystem(externalSystem.getProxy(params));
		if (object != null) proxy.setObject(object.getProxy(params));
		proxy.setAlias(alias);
		return proxy;
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ExternalSystemAlias populate(BasePersistentProvider provider, BaseProxy proxy) {
		final ExternalSystemAliasProxy internal = (ExternalSystemAliasProxy) proxy;
		externalSystem = Util.proxyHasId(internal.getExternalSystem()) ? provider.find(ExtSystem.class, internal.getExternalSystem()) : null;
		final Class objType = Util.proxyHasId(internal.getObject()) ? provider.mapper().entityClass(internal.getObject().getClass()) : null;
		object = (Entity) (objType != null ? provider.find(objType, internal.getObject().getId()) : null);
		alias = internal.getAlias();
		return this;
	}
	
	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper()
			.add(ExternalSystemAliasProxy.EXTERNAL_SYSTEM, externalSystem)
			.add(ExternalSystemAliasProxy.OBJECT, object)
			.add(ExternalSystemAliasProxy.ALIAS, alias);
	}
}
