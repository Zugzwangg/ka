/*
 * Tez Tour core library.
 * Copyright(c) 2017-2018. 
 * October 18, 2017.
 * tez-tour 
 * 
 */
package com.cleverforms.teztour;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.cleverforms.comms.server.RESTConnectorImpl;
import com.cleverforms.ics.db.model.dictionary.City;
import com.cleverforms.ics.db.model.dictionary.Country;
import com.cleverforms.ics.db.model.dictionary.Region;
import com.cleverforms.ics.db.model.pub.ExtSystem;
import com.cleverforms.ics.shared.dictionary.proxy.CityProxy;
import com.cleverforms.ics.shared.dictionary.proxy.ConsumerProxy;
import com.cleverforms.iss.server.integration.marker.HasBooking;
import com.cleverforms.iss.server.model.hotel.ExternalSystemAlias;
import com.cleverforms.iss.server.model.hotel.Hotel;
import com.cleverforms.iss.server.model.hotel.HotelClass;
import com.cleverforms.iss.server.model.hotel.HotelFood;
import com.cleverforms.iss.server.provider.ISSPersistentProvider;
import com.cleverforms.iss.server.travel.TravelConnector;
import com.cleverforms.iss.shared.hotel.model.RoomTypeModel;
import com.cleverforms.iss.shared.hotel.proxy.HotelProxy;
import com.cleverforms.iss.shared.travel.model.OneWayTicketAvailability;
import com.cleverforms.iss.shared.travel.model.TravelBookModel;
import com.cleverforms.iss.shared.travel.model.TravelItineraryModel;
import com.cleverforms.iss.shared.travel.model.TravelOrderModel;
import com.cleverforms.iss.shared.travel.model.TwoWayTicketAvailability;
import com.cleverforms.iss.shared.travel.proxy.TravelBookInfo;
import com.cleverforms.iss.shared.travel.proxy.TravelItineraryProxy;
import com.cleverforms.iss.shared.travel.proxy.TravelOrderProxy;
import com.cleverforms.iss.shared.travel.proxy.TravelSearchRequestProxy;
import com.sencha.gxt.core.client.util.Util;

@SuppressWarnings("deprecation")
public class TezTourConnector extends RESTConnectorImpl
		implements TravelConnector, InitializingBean, HasBooking<TravelOrderProxy, TravelBookModel> {

	@Autowired
	protected ISSPersistentProvider provider;
	
	protected final SimpleDateFormat bookingFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm");
	protected final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
	protected Map<String, Long> currencies;
	protected Map<AbstractMap.SimpleEntry<Short, Short>, Long> accommodations;
	private String currencyIds;
	private String accommodationIds;

	public TezTourConnector() {
		currencies = new HashMap<>();
		accommodations = new HashMap<>();
	}

	@Override
	public TezTourConnector clone() {
		return (TezTourConnector) super.clone();
	}

	public void setCurrencyIds(String ids) {
		this.currencyIds = ids;
	}

	public void setAccommodationIds(String accommodationIds) {
		this.accommodationIds = accommodationIds;
	}

	@Override
	@Transactional
	public List<TravelItineraryProxy> getTravelItineraries(TravelSearchRequestProxy searchModel) {
		List<TravelItineraryProxy> travelItineraries = new ArrayList<>();
		
		Long currency = currencies.get(searchModel.getCurrency().getCode());
		if (currency == null) {
			return travelItineraries;
		}

		String hotelClass = (String) provider.getSingleResult("select e.alias from ExternalSystemAlias e where e.object = ?0", provider.find(HotelClass.class, searchModel.getHotelClass().getId()));
		if(hotelClass == null) {
			return travelItineraries;
		}
		
		Long accommodation = accommodations.get(
				new AbstractMap.SimpleEntry<Short, Short>(searchModel.getAdultCount(), searchModel.getChildCount()));
		if (accommodation == null) {
			return travelItineraries;
		}

		HotelFood hotelFood = provider.find(HotelFood.class, searchModel.getHotelFood().getId());
		String food = (String) provider.getSingleResult("select e.alias from ExternalSystemAlias e where e.object = ?0", hotelFood);
		if(food == null) {
			return travelItineraries;
		}
		
		String city = (String) provider.getSingleResult("select e.alias from ExternalSystemAlias e where e.object = ?0", provider.find(City.class, searchModel.getCity().getId()));
		if(city == null || !city.contains(";")) {
			return travelItineraries;
		} else {
			int index = city.indexOf(";");
			if(index == -1 || index == city.length() - 1 || city.isEmpty()) {
				return travelItineraries;
			} else {
				city = city.substring(index + 1, city.length());
			}
		}
		
		String country = (String) provider.getSingleResult("select e.alias from ExternalSystemAlias e where e.object = ?0", provider.find(Country.class, searchModel.getCountry().getId()));
		if(country == null) {
			return travelItineraries;
		}

		Set<String> regions = new HashSet<>();
		Set<Long> destinationCityIds = new HashSet<>();
		for (CityProxy destinationCity : searchModel.getDestinationCities()) {
			Region region = ((City)provider.find(City.class, destinationCity.getId())).getRegion();
			if(region != null) {
				String ttRegionAlias = (String) provider.getSingleResult("select e.alias from ExternalSystemAlias e where e.object = ?0", region);
				if(ttRegionAlias != null) {
					regions.add(ttRegionAlias);
					destinationCityIds.add(destinationCity.getId());
				}
			}
		}
		if(regions.isEmpty()) {
			regions.add("1974");
		}

		Set<String> hotels = new HashSet<>();
		for (HotelProxy hotel : searchModel.getHotels()) {
			String ttHotelAlias = (String) provider.getSingleResult("select e.alias from ExternalSystemAlias e where e.object = ?0", provider.find(Hotel.class, hotel.getId()));
			if(ttHotelAlias != null) {
				hotels.add(ttHotelAlias);
			}
		}
		if(hotels.isEmpty()) {
			if(!searchModel.getHotels().isEmpty()) {
				return travelItineraries;
			} else {
				hotels.add("42736");
				hotels.add("42709");
				hotels.add("9000369");
			}
		}

		// TODO
		//int airport = 1104;
		//String airport = (String) provider.getSingleResult("select e.alias from ExternalSystemAlias e where e.object = ?0", provider.find(Airport.class, searchModel.getAirport().getId()));

		String childBirthday1 = null, childBirthday2 = null;
		if (searchModel.getChildBirthdays() != null) {
			String[] childBirthdays = searchModel.getChildBirthdays().split(",");
			if (childBirthdays.length == 1) {
				childBirthday1 = childBirthdays[0];
			} else if (childBirthdays.length == 2) {
				childBirthday1 = childBirthdays[0];
				childBirthday2 = childBirthdays[1];
			} else {
				return travelItineraries;
			}
		}

		try {
			// TODO Think if TourSearch should be also used
			String url = String.format(
					"https://www.tez-tour.com/"
							+ "tariffsearch/getResult?priceMin=%d&priceMax=%d&currency=%d&nightsMin=%d&nightsMax=%d"
							+ "&hotelClassId=%s&accommodationId=%d&rAndBId=%s&tourType=%s&locale=ru&cityId=%s&countryId=%s"
							+ "&after=%s&before=%s&hotelClassBetter=true&rAndBBetter=true&xml=true",
					(int) searchModel.getMinPrice(), (int) searchModel.getMaxPrice(), currency,
					searchModel.getMinNights(), searchModel.getMaxNights(), hotelClass, accommodation, food,
					searchModel.getTourType(), city, country, sdf.format(searchModel.getAfter()),
					sdf.format(searchModel.getBefore()));
			for(String region : regions) {
				url += "&tourId=" + region;
			}
			for(String hotel : hotels) {
				url += "&hotelId=" + hotel;
			}
			if(childBirthday1 != null) {
				url += "&childBirthday1=" + childBirthday1;
			}
			if(childBirthday2 != null) {
				url += "&childBirthday2=" + childBirthday2;
			}
			ResponseEntity<String> response = restTemplate().getForEntity(url, String.class);
			DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = documentBuilder.parse(new InputSource(new StringReader(response.getBody())));
			XPath xPath = XPathFactory.newInstance().newXPath();
			
			NodeList items = (NodeList) xPath.compile("/searchResult/data/item").evaluate(document,
					XPathConstants.NODESET);
			for (int i = 0; i < items.getLength(); i++) {
				TravelItineraryProxy travelItinerary = new TravelItineraryModel((long) i, "TEZ TOUR");
				Element item = (Element) items.item(i);
				String checkInString = item.getElementsByTagName("checkIn").item(0).getTextContent();
				Date checkIn = sdf.parse(checkInString);
				travelItinerary.setCheckIn(checkIn);
				Date checkOut = sdf.parse(
						item.getElementsByTagName("checkOut").item(0).getTextContent() + checkInString.substring(5));
				if (!checkOut.after(checkIn)) {
					checkOut.setYear(checkOut.getYear() + 1);
				}
				travelItinerary.setCheckOut(checkOut);
				
				// Check if the hotel of a result item is from a city of a request
				String hotelId = ((Element) item.getElementsByTagName("hotel").item(0)).getElementsByTagName("id").item(0).getTextContent();
				ExternalSystemAlias alias = null;
				for(Object e : provider.getResultList("from ExternalSystemAlias e where e.alias = '" + hotelId + "'", 0, 0)) {
					if(((ExternalSystemAlias)e).getObject() instanceof Hotel) {
						alias = (ExternalSystemAlias) e;
						break;
					}
				}
				if(!destinationCityIds.isEmpty() && (alias == null || !destinationCityIds.contains(((Hotel)alias.getObject()).getCity().getId()))) {
					continue;
				}
				travelItinerary.setHotel((HotelProxy) alias.getObject().getProxy());

				/*String foodId = ((Element) item.getElementsByTagName("pansion").item(0)).getElementsByTagName("id").item(0).getTextContent();
				alias = null;
				for(Object e : provider.getResultList("from ExternalSystemAlias e where e.alias = '" + foodId + "'", 0, 0)) {
					if(((ExternalSystemAlias)e).getObject() instanceof Hotel) {
						alias = (ExternalSystemAlias) e;
						break;
					}
				}
				if(alias == null) {
					continue;
				}
				travelItinerary.setFood((HotelFoodProxy) alias.getObject().getProxy());*/
				travelItinerary.setFood(hotelFood.getProxy());
				
				/*String roomTypeId = ((Element) item.getElementsByTagName("hotelRoomType").item(0)).getElementsByTagName("id").item(0).getTextContent();
				alias = null;
				for(Object e : provider.getResultList("from ExternalSystemAlias e where e.alias = '" + roomTypeId + "'", 0, 0)) {
					if(((ExternalSystemAlias)e).getObject() instanceof Hotel) {
						alias = (ExternalSystemAlias) e;
						break;
					}
				}
				if(alias == null) {
					continue;
				}
				travelItinerary.setRoomType((RoomTypeProxy) alias.getObject().getProxy());*/
				travelItinerary.setRoomType(new RoomTypeModel(2l));
				
				// NightCount
				travelItinerary.setNightCount(
						Short.parseShort(item.getElementsByTagName("nightCount").item(0).getTextContent()));
				Element price = (Element) item.getElementsByTagName("price").item(0);
				
				String currencyId = price.getElementsByTagName("currencyId").item(0).getTextContent();
				for(Entry<String, Long> entry : currencies.entrySet()) {
					if(Util.equalWithNull(String.valueOf(entry.getValue()), currencyId)) {
						travelItinerary.setCurrencyCode(entry.getKey());
						break;
					}
				}
				
				// Price
				travelItinerary
						.setPrice(Double.parseDouble(price.getElementsByTagName("total").item(0).getTextContent()));
				
				Element ageGroup = (Element) item.getElementsByTagName("ageGroupType").item(0);
				// AdultCount
				NodeList adults = ageGroup.getElementsByTagName("adult");
				if (adults.getLength() == 0) {
					travelItinerary.setAdultCount((short) 0);
				} else {
					travelItinerary.setAdultCount(Short.parseShort(
							((Element) adults.item(0)).getElementsByTagName("count").item(0).getTextContent()));
				}
				NodeList bigChildren = ageGroup.getElementsByTagName("bigChild");
				short bigChildCount;
				if (bigChildren.getLength() == 0) {
					bigChildCount = 0;
				} else {
					bigChildCount = Short.parseShort(
							((Element) bigChildren.item(0)).getElementsByTagName("count").item(0).getTextContent());
				}
				short smallChildCount;
				NodeList smallChildren = ageGroup.getElementsByTagName("smallChild");
				if (smallChildren.getLength() == 0) {
					smallChildCount = 0;
				} else {
					smallChildCount = Short.parseShort(
							(((Element) smallChildren.item(0)).getElementsByTagName("count").item(0).getTextContent()));
				}
				travelItinerary.setChildCount((short) (bigChildCount + smallChildCount));

				String bookingUrl = xPath.compile("bookingUrl/bookingUrl/url").evaluate(item);
				travelItinerary.setBookingParams(bookingUrl);
				
				Element seatSetPair = (Element) ((Element) item.getElementsByTagName("seatSets").item(0)).getElementsByTagName("seatSetPair").item(0);
				Element to = (Element) seatSetPair.getElementsByTagName("to").item(0);
				Element from = (Element) seatSetPair.getElementsByTagName("from").item(0);

				travelItinerary.setTicketAvailability(new TwoWayTicketAvailability(
						new OneWayTicketAvailability(((Element)to.getElementsByTagName("first").item(0)).getElementsByTagName("seatSet").item(0).getTextContent(),
								((Element)to.getElementsByTagName("business").item(0)).getElementsByTagName("seatSet").item(0).getTextContent(),
								((Element)to.getElementsByTagName("econom").item(0)).getElementsByTagName("seatSet").item(0).getTextContent(),
								((Element)to.getElementsByTagName("premiumEconom").item(0)).getElementsByTagName("seatSet").item(0).getTextContent()),
						new OneWayTicketAvailability(((Element)from.getElementsByTagName("first").item(0)).getElementsByTagName("seatSet").item(0).getTextContent(),
								((Element)from.getElementsByTagName("business").item(0)).getElementsByTagName("seatSet").item(0).getTextContent(),
								((Element)from.getElementsByTagName("econom").item(0)).getElementsByTagName("seatSet").item(0).getTextContent(),
								((Element)from.getElementsByTagName("premiumEconom").item(0)).getElementsByTagName("seatSet").item(0).getTextContent())));
				
				travelItineraries.add(travelItinerary);
			}
		} catch (RestClientException | NumberFormatException | XPathExpressionException | DOMException
				| ParserConfigurationException | SAXException | IOException | ParseException e) {
			e.printStackTrace();
		}

		return travelItineraries;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		for (String c : currencyIds.split(",")) {
			String[] s = c.split("=");
			currencies.put(s[0], Long.parseLong(s[1]));
		}

		for (String a : accommodationIds.split(",")) {
			String[] s1 = a.split("=");
			String[] s2 = s1[0].split(":");
			accommodations.put(
					new AbstractMap.SimpleEntry<Short, Short>(Short.parseShort(s2[0]), Short.parseShort(s2[1])),
					Long.parseLong(s1[1]));
		}
	}

	@Override
	@Transactional
	public List<TravelOrderProxy> booking(TravelBookModel bookModel) {
		List<TravelOrderProxy> travelOrders = new ArrayList<>();
		for (TravelBookInfo tbi : bookModel.getBookingItems()) {
			String bookingUrl = tbi.getBookingUrl();

			int index = bookingUrl.indexOf("rar=");
			if (index == -1) {
				return travelOrders;
			}
			index += 4;
			String rar;
			int tempIndex = bookingUrl.indexOf("&", index);
			if (tempIndex == -1) {
				rar = bookingUrl.substring(index);
			} else {
				rar = bookingUrl.substring(index, tempIndex);
			}
			index = bookingUrl.indexOf("rdr=");
			if (index == -1) {
				return travelOrders;
			}
			index += 4;
			String rdr;
			tempIndex = bookingUrl.indexOf("&", index);
			if (tempIndex == -1) {
				rdr = bookingUrl.substring(index);
			} else {
				rdr = bookingUrl.substring(index, tempIndex);
			}
			index = bookingUrl.indexOf("ltt=");
			if (index == -1) {
				return travelOrders;
			}
			index += 4;
			String ltt;
			tempIndex = bookingUrl.indexOf("&", index);
			if (tempIndex == -1) {
				ltt = bookingUrl.substring(index);
			} else {
				ltt = bookingUrl.substring(index, tempIndex);
			}
			index = bookingUrl.indexOf("ftt=");
			if (index == -1) {
				return travelOrders;
			}
			index += 4;
			String ftt;
			tempIndex = bookingUrl.indexOf("&", index);
			if (tempIndex == -1) {
				ftt = bookingUrl.substring(index);
			} else {
				ftt = bookingUrl.substring(index, tempIndex);
			}
			index = bookingUrl.indexOf("cResId=");
			if (index == -1) {
				return travelOrders;
			}
			index += 7;
			String cResId;
			tempIndex = bookingUrl.indexOf("&", index);
			if (tempIndex == -1) {
				cResId = bookingUrl.substring(index);
			} else {
				cResId = bookingUrl.substring(index, tempIndex);
			}
			index = bookingUrl.indexOf("cFlyIds=");
			if (index == -1) {
				return travelOrders;
			}
			index += 8;
			String cFlyIds;
			tempIndex = bookingUrl.indexOf("&", index);
			if (tempIndex == -1) {
				cFlyIds = bookingUrl.substring(index);
			} else {
				cFlyIds = bookingUrl.substring(index, tempIndex);
			}
			index = bookingUrl.indexOf("priceOfferId=");
			if (index == -1) {
				return travelOrders;
			}
			index += 13;
			String priceOfferId;
			tempIndex = bookingUrl.indexOf("&", index);
			if (tempIndex == -1) {
				priceOfferId = bookingUrl.substring(index);
			} else {
				priceOfferId = bookingUrl.substring(index, tempIndex);
			}
			index = bookingUrl.indexOf("depCity=");
			if (index == -1) {
				return travelOrders;
			}
			index += 8;
			String depCity;
			tempIndex = bookingUrl.indexOf("&", index);
			if (tempIndex == -1) {
				depCity = bookingUrl.substring(index);
			} else {
				depCity = bookingUrl.substring(index, tempIndex);
			}

			String login = "ButovAV";
			String password = "ZLNRc45";
			String authenticateUrl = "http://xml.tez-tour.com/xmlgate/auth_data.jsp?j_login_request=1&j_login=" + login
					+ "&j_passwd=" + password;
			ResponseEntity<String> response3 = restTemplate().getForEntity(authenticateUrl, String.class);
			try {
				DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				Document document = documentBuilder.parse(new InputSource(new StringReader(response3.getBody())));
				XPath xPath = XPathFactory.newInstance().newXPath();

				String sessionId = xPath.compile("/auth_data/sessionId").evaluate(document);

				StringBuilder url = new StringBuilder(
						"http://xml.tez-tour.com/xmlgate/order/orderFromOfferId.jsp?tariffDepCityId=");
				url.append(depCity);
				url.append("&firstTransferType=");
				url.append(ftt);
				url.append("&lastTransferType=");
				url.append(ltt);
				url.append("&resortArrivalRegionId=");
				url.append(rar);
				url.append("&resortDepartureRegionId=");
				url.append(rdr);
				url.append("&resTariffs=");
				url.append(cResId);
				url.append("&priceOfferId=");
				url.append(priceOfferId);
				url.append("&flyTariffs=");
				url.append(cFlyIds);
				url.append("&aid=");
				url.append(sessionId);

				ResponseEntity<String> response = restTemplate().getForEntity(url.toString(), String.class);

				document = documentBuilder.parse(new InputSource(new StringReader(response.getBody())));

				NodeList tourists = (NodeList) xPath.compile("/order/Tourist").evaluate(document,
						XPathConstants.NODESET);
				Iterator<ConsumerProxy> consumers = bookModel.getConsumers().iterator();
				for (int i = 0; i < tourists.getLength(); i++) {
					Element tourist = (Element) tourists.item(i);

					ConsumerProxy consumer =  consumers.next();
					
					tourist.getElementsByTagName("surname").item(0).setTextContent(consumer.getLastName());
					tourist.getElementsByTagName("name").item(0).setTextContent(consumer.getName());
					tourist.getElementsByTagName("birthday").item(0).setTextContent(sdf.format(consumer.getBirthday()));
					String citizenship = consumer.getDocument().substring(0, consumer.getDocument().indexOf(";"));
					Country c = (Country) provider.getSingleResult("from Country c left join c.properties p where index(p) = 'IATA_CODE' and p = ?0", citizenship);
					if(c == null) {
						return travelOrders;
					} else {
						tourist.getElementsByTagName("nationality").item(0).setTextContent(((ExternalSystemAlias) provider.getSingleResult("from ExternalSystemAlias e where e.externalSystem = ?0 and e.object = ?1", provider.find(ExtSystem.class, 26l), c)).getAlias());
					}
				}

				Node order = (Node) xPath.compile("/order").evaluate(document, XPathConstants.NODE);
				Element timeLimit = document.createElement("TimeLimit");
				Element type = document.createElement("type");
				type.setTextContent("128770");
				Element value = document.createElement("value");
				Calendar c = Calendar.getInstance();
				c.add(Calendar.DATE, 1);
				value.setTextContent(bookingFormat.format(c.getTime()));
				timeLimit.appendChild(type);
				timeLimit.appendChild(value);
				order.appendChild(timeLimit);
				TransformerFactory tf = TransformerFactory.newInstance();
				Transformer transformer = tf.newTransformer();

				DOMSource source = new DOMSource(document);
				StringWriter writer = new StringWriter();
				StreamResult result = new StreamResult(writer);

				transformer.transform(source, result);

				String bookXml = writer.toString();
				
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_XML);

				HttpEntity<String> entity = new HttpEntity<String>(bookXml, headers);
				ResponseEntity<String> response2 = restTemplate()
						.postForEntity("http://xml.tez-tour.com/xmlgate/order/book", entity, String.class);

				document = documentBuilder.parse(new InputSource(new StringReader(response2.getBody())));

				TravelOrderProxy top = new TravelOrderModel();
				top.setNumber(xPath.compile("/booking-result/orderId").evaluate(document));
				travelOrders.add(top);
			} catch (XPathExpressionException | DOMException | ParserConfigurationException | SAXException | IOException
					| TransformerException e) {
				return travelOrders;
			}
		}
		return travelOrders;
	}
}
