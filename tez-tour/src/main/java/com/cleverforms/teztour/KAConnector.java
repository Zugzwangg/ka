/*
 * Tez Tour core library.
 * Copyright(c) 2017-2018. 
 * October 18, 2017.
 * tez-tour 
 * 
 */
package com.cleverforms.teztour;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.cleverforms.comms.server.RESTConnectorImpl;
import com.cleverforms.ics.shared.dictionary.proxy.CityProxy;
import com.cleverforms.iss.server.model.travel.Tour;
import com.cleverforms.iss.server.provider.ISSPersistentProvider;
import com.cleverforms.iss.server.travel.TravelConnector;
import com.cleverforms.iss.shared.hotel.proxy.HotelProxy;
import com.cleverforms.iss.shared.travel.model.OneWayTicketAvailability;
import com.cleverforms.iss.shared.travel.model.TravelBookModel;
import com.cleverforms.iss.shared.travel.model.TravelItineraryModel;
import com.cleverforms.iss.shared.travel.model.TwoWayTicketAvailability;
import com.cleverforms.iss.shared.travel.proxy.TravelItineraryProxy;
import com.cleverforms.iss.shared.travel.proxy.TravelOrderProxy;
import com.cleverforms.iss.shared.travel.proxy.TravelSearchRequestProxy;

public class KAConnector extends RESTConnectorImpl implements TravelConnector {

	@Autowired
	protected ISSPersistentProvider provider;

	@Override
	public boolean checkConnector() {
		return true;
	}
	
	@Override
	public KAConnector clone() {
		return (KAConnector) super.clone();
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<TravelItineraryProxy> getTravelItineraries(TravelSearchRequestProxy searchModel) {
		List<TravelItineraryProxy> travelItineraries = new ArrayList<>();
		
		String query = "from Tour where checkIn >= ?0 and checkIn <= ?1 and adultCount = ?2 and childCount = ?3 and duration >= ?4"
				+ " and duration <= ?5 and hotelFood.id = ?6 and price <= ?7 and price >= ?8 and currency.code = ?9";
		List<Tour> tours;
		if(!searchModel.getHotels().isEmpty()) {
			query += " and hotel.id in (?10)";
			Set<Long> hotelIds = new HashSet<>();
			for(HotelProxy hotel : searchModel.getHotels()) {
				hotelIds.add(hotel.getId());
			}
			tours = provider.getResultList(query, 0, -1, searchModel.getAfter(), searchModel.getBefore(), searchModel.getAdultCount(), searchModel.getChildCount(), searchModel.getMinNights(),
					searchModel.getMaxNights(), searchModel.getHotelFood().getId(), searchModel.getMaxPrice(), searchModel.getMinPrice(), searchModel.getCurrency().getCode(), hotelIds);
		} else if(!searchModel.getDestinationCities().isEmpty()) {
			query += " and hotel.city.id in (?10)";
			Set<Long> cityIds = new HashSet<>();
			for(CityProxy city : searchModel.getDestinationCities()) {
				cityIds.add(city.getId());
			}
			tours = provider.getResultList(query, 0, -1, searchModel.getAfter(), searchModel.getBefore(), searchModel.getAdultCount(), searchModel.getChildCount(), searchModel.getMinNights(),
					searchModel.getMaxNights(), searchModel.getHotelFood().getId(), searchModel.getMaxPrice(), searchModel.getMinPrice(), searchModel.getCurrency().getCode(), cityIds);
		} else {
			query += " and hotel.city.country.id = ?10";
			tours = provider.getResultList(query, 0, -1, searchModel.getAfter(), searchModel.getBefore(), searchModel.getAdultCount(), searchModel.getChildCount(), searchModel.getMinNights(),
					searchModel.getMaxNights(), searchModel.getHotelFood().getId(), searchModel.getMaxPrice(), searchModel.getMinPrice(), searchModel.getCurrency().getCode(), searchModel.getCountry().getId());
		}
		for(Tour tour : tours) {
			TravelItineraryProxy travelItinerary = new TravelItineraryModel(0L, "KA");
			travelItinerary.setAdultCount(tour.getAdultCount());
			travelItinerary.setChildCount(tour.getChildCount());
			travelItinerary.setCheckIn(tour.getCheckIn());
			Calendar c = Calendar.getInstance();
			c.setTime(tour.getCheckIn());
			c.add(Calendar.DATE, tour.getDuration());
			travelItinerary.setCheckOut(c.getTime());
			travelItinerary.setCurrencyCode(tour.getCurrency().getCode());
			travelItinerary.setFood(tour.getHotelFood().getProxy());
			travelItinerary.setHotel(tour.getHotel().getProxy());
			travelItinerary.setNightCount(tour.getDuration());
			travelItinerary.setPrice(tour.getPrice());
			travelItinerary.setRoomType(tour.getRoomType().getProxy());
			travelItinerary.setTicketAvailability(new TwoWayTicketAvailability(new OneWayTicketAvailability("AVAILABLE", "AVAILABLE", "AVAILABLE", "AVAILABLE"), new OneWayTicketAvailability("AVAILABLE", "AVAILABLE", "AVAILABLE", "AVAILABLE")));
			travelItineraries.add(travelItinerary);
		}
			
		return travelItineraries;
	}

	@Override
	public List<TravelOrderProxy> booking(TravelBookModel bookModel) {
		return null;
	}
}
