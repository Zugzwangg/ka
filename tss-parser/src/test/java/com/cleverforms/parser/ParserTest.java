package com.cleverforms.parser;

import java.io.File;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import com.cleverforms.comms.server.WithLogger;
import com.cleverforms.iss.server.provider.ISSPersistentProvider;
import com.cleverforms.parser.exchangerate.ExchangeRateParser;

@Rollback(true)
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/applicationContext.xml" })
public class ParserTest extends WithLogger {
	
	@Autowired
	public ISSPersistentProvider provider;

	@Test
	@Rollback(false)
	public void testOfflineFileListeners() {
		try {
			Thread.sleep(Long.MAX_VALUE);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		debug("end");
	}

	@Test
	public void testExchangeParser() {
		try {
			new ExchangeRateParser().parse(new File("C:\\Users\\perekladov\\Desktop\\currency.xml"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
