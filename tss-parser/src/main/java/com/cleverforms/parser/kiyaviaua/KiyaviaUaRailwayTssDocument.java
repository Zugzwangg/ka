package com.cleverforms.parser.kiyaviaua;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class KiyaviaUaRailwayTssDocument extends KiyaviaUaRailwayDocument {
	public KiyaviaUaRailwayTssDocument(File file) throws ParserConfigurationException, FactoryConfigurationError, SAXException, IOException {
		super(file);
	}

	/*-public String getType() throws XPathExpressionException {
		return "RAIL_KA.UA";
	}

	public String getNumber() throws XPathExpressionException {
		return getBookingId();
	}

	public Set<DocumentRoute> getRoutes() throws XPathExpressionException, DOMException, ParsingException, ParseException {
		Set<DocumentRoute> routes = new HashSet<>();

		DocumentRoute route = new DocumentRoute();
		// TODO Set real station from, station to, arrival date, departure date
		route.stationFrom = "СЕРЕЖА";
		route.stationTo = "СЕРЕЖА";
		route.arrivalDate = new Date();
		route.departureDate = new Date();
		route.documentNumber = getBookingId();
		route.isElectronic = true;
		// TODO Set real train number
		route.trainNumber = "";
		for (Good good : getGoods()) {
			DocumentTicket ticket = new DocumentTicket();
			ticket.number = getBookingId();
			ticket.passenger = new DocumentConsumer(good.firstName, good.lastName);
			ticket.seatNumber = good.seatNumber;
			ticket.wagonNumber = good.carNumber;
			ticket.baseAmount = good.amount;
			ticket.documentType = RailwayTicketType.TRAVEL;
			route.tickets.add(ticket);
			if (route.tickets.size() == 1) {
				ticket.taxes.put("COMMISSION", getServiceFee());
			}
		}

		routes.add(route);

		return routes;
	}

	public Date getDate() throws XPathExpressionException, ParseException {
		return getTicketingDate();
	}

	public OrderState getOperationType() throws XPathExpressionException, ParsingException {
		if (getOperation().equals("sale")) {
			return OrderState.SALE;
		} else if (getOperation().equals("refund")) {
			return OrderState.REFUND;
		} else {
			throw new ParsingException("Unknown operation type");
		}
	}

	public Set<String> getVoidTicketNumbers() throws XPathExpressionException, ParsingException {
		Set<String> voidTicketNumbers = new HashSet<>();
		voidTicketNumbers.add(getLocator());
		return voidTicketNumbers;
	}*/
}
