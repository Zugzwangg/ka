package com.cleverforms.parser;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public abstract class XmlUtils {
	public static String readTextContent(Element item, String tagName) {
		Node node = item.getElementsByTagName(tagName).item(0).getFirstChild();
		return node == null ? "" : node.getNodeValue();
	}
}
