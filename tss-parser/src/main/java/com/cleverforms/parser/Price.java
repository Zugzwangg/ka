package com.cleverforms.parser;

public class Price {
	public double amount;
	public String currency;

	public Price() {
	}

	public Price(double amount, String currency) {
		this.amount = amount;
		this.currency = currency;
	}

	@Override
	public String toString() {
		return amount + " " + currency;
	}
}
