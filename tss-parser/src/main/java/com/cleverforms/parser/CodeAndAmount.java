package com.cleverforms.parser;

public class CodeAndAmount {
	public String code;
	public double amount;

	public CodeAndAmount(String code, double amount) {
		this.code = code;
		this.amount = amount;
	}
}