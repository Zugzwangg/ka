package com.cleverforms.parser;

public class NumberAndPrice {
	public String number;
	public Price price;

	public NumberAndPrice(String number, Price price) {
		this.number = number;
		this.price = price;
	}
}