/*
 * Health insurance library (HI).
 * Copyright(c) 2010-2018.
 * December 24, 2013.
 * hi 
 * 
 */
package com.cleverforms.svd.core.api.model.request;

import java.util.Date;

import com.cleverforms.comms.server.model.BaseData;
import com.cleverforms.comms.shared.util.date.DateTime;
import com.cleverforms.comms.shared.util.date.DateTime.Unit;
import com.thoughtworks.xstream.annotations.XStreamAlias;
/**
 * 
 * @author Sergey Titarchuk
 *
 */
public class CalculateRequestData extends BaseData {
	
	/** The serial version UID*/
	private static final long serialVersionUID = -1094860983686759643L;
	
	protected long country;
	protected long amount;
	@XStreamAlias("start-date")
	protected Date startDate;
	@XStreamAlias("end-date")
	protected Date endDate;
	protected long purposes; 
	
	public CalculateRequestData() {
	}
	
	/**
	 * @return the country
	 */
	public long getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(long country) {
		this.country = country;
	}

	/**
	 * @return the amount
	 */
	public long getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(long amount) {
		this.amount = amount;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate != null && startDate.after(new Date()) ? startDate : DateTime.add(new Date(), 1, Unit.DAY);
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate != null && endDate.after(getStartDate()) ? endDate : DateTime.add(getStartDate(), 2, Unit.DAY);
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the purposes
	 */
	public long getPurposes() {
		return purposes;
	}

	/**
	 * @param purposes the purposes to set
	 */
	public void setPurposes(long purposes) {
		this.purposes = purposes;
	}

	@Override
	public String toString() {
		return "CalculateRequestData[country = " + country
				+ ", amount = " + amount
				+ ", startDate = " + startDate
				+ ", endDate = " + endDate
				+ ", purposes = " + purposes
				+ "]";
	}
	
}
