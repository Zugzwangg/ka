/*
 * Health insurance library (HI).
 * Copyright(c) 2010-2018.
 * December 24, 2013.
 * hi 
 * 
 */
package com.cleverforms.svd.core.api.insurance;

import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;
import com.cleverforms.svd.core.api.config.FieldConfig;
import com.cleverforms.svd.core.api.config.FormConfig;
import com.cleverforms.svd.core.api.model.InsurancePrograms;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
/**
 * 
 * @author Sergey Titarchuk
 *
 */
@XStreamAlias(value = "insurance")
public class InsurancePayload extends FormConfig {

	/** The serial version UID */
	private static final long serialVersionUID = -1669092865389529210L;

	/** Key to amount property */
	public static final String AMOUNT = "amount";
	
	/** Key to consumers property */
	public static final String CONSUMERS = "consumers";
	
	/** Key to country property */
	public static final String COUNTRY = "country";
	
	/** Key to customer property */
	public static final String CUSTOMER = "customer";
	
	/** Key to description property */
	public static final String DESCRIPTION = "description";
	
	/** Key to end property */
	public static final String END = "end";
	
	/** Key to price property */
	public static final String PRICE = "price";

	/** Key to programs property */
	public static final String PROGRAMS = "programs";
	
	/** Key to purposes property */
	public static final String PURPOSES = "purposes";
	
	/** Key to sector property */
	public static final String SECTOR = "sector";

	/** Key to age query */
	public static final String SQL_AGE = "age";

	/** Key to amount query */
	public static final String SQL_AMOUNT = "amount";

	/** Key to start property */
	public static final String START = "start";

	/** Key to tariff property */
	public static final String TARIFF = "tariff";

	@XStreamAsAttribute
	private long sector;
	private String description;
	private FieldConfig country;
	private FieldConfig customer;
	private InsurancePrograms programs;
	private FieldConfig amount; 
	private FieldConfig start;
	private FieldConfig end;
	private FieldConfig purposes;
	private InsuredConfig consumers;
	private FieldConfig tariff;
	private FieldConfig price;

	public InsurancePayload() {}
	
	public InsurancePayload(long form, long sector, String description) {
		super(form);
		this.sector = sector;
		this.description = description;
	}
	
	public long getSector() {
		return sector;
	}
	
	public void setSector(long sector) {
		this.sector = sector;
	}
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the country
	 */
	public FieldConfig getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(FieldConfig country) {
		this.country = country;
	}

	/**
	 * @return the customer
	 */
	public FieldConfig getInsurer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setInsurer(FieldConfig insurer) {
		this.customer = insurer;
	}

	/**
	 * @return the programs
	 */
	public InsurancePrograms getPrograms() {
		return programs;
	}

	/**
	 * @param programs the programs to set
	 */
	public void setPrograms(InsurancePrograms programs) {
		this.programs = programs;
	}

	/**
	 * @return the amount
	 */
	public FieldConfig getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(FieldConfig amount) {
		this.amount = amount;
	}

	/**
	 * @return the start
	 */
	public FieldConfig getStart() {
		return start;
	}

	/**
	 * @param start the start to set
	 */
	public void setStart(FieldConfig start) {
		this.start = start;
	}

	/**
	 * @return the end
	 */
	public FieldConfig getEnd() {
		return end;
	}

	/**
	 * @param end the end to set
	 */
	public void setEnd(FieldConfig end) {
		this.end = end;
	}

	/**
	 * @return the purposes
	 */
	public FieldConfig getPurposes() {
		return purposes;
	}

	/**
	 * @param purposes the purposes to set
	 */
	public void setPurposes(FieldConfig purposes) {
		this.purposes = purposes;
	}

	/**
	 * @return the consumers
	 */
	public InsuredConfig getConsumers() {
		return consumers;
	}

	/**
	 * @param consumers the consumers to set
	 */
	public void setConsumers(InsuredConfig consumers) {
		this.consumers = consumers;
	}

	/**
	 * @return the tariff
	 */
	public FieldConfig getTariff() {
		return tariff;
	}

	/**
	 * @param tariff the tariff to set
	 */
	public void setTariff(FieldConfig tariff) {
		this.tariff = tariff;
	}

	/**
	 * @return the price
	 */
	public FieldConfig getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(FieldConfig price) {
		this.price = price;
	}

	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper()
			.add(DESCRIPTION, description)
			.add(SECTOR, sector)
			.add(COUNTRY, country)
			.add(CUSTOMER, customer)
			.add(PROGRAMS, programs)
			.add(AMOUNT, amount)
			.add(START, start)
			.add(END, end)
			.add(PURPOSES, purposes)
			.add(CONSUMERS, consumers)
			.add(TARIFF, tariff)
			.add(PRICE, price)
			.add(QUERIES, getQueries());
	}
}
