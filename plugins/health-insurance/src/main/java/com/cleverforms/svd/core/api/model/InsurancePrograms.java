/*
 * Health insurance library (HI).
 * Copyright(c) 2010-2018.
 * December 25, 2013.
 * hi 
 * 
 */
package com.cleverforms.svd.core.api.model;

import com.cleverforms.comms.server.model.ListWrapper;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
/**
 * 
 * @author Sergey Titarchuk
 *
 */
public class InsurancePrograms extends ListWrapper<InsuranceProgram> {
	
	/** The serial version UID */
	private static final long serialVersionUID = -7066026647664507533L;

	@XStreamAsAttribute
	private long field;
	
	public InsurancePrograms() {
		this(0);
	}

	public InsurancePrograms(long field) {
		this.field = field;
	}

	/**
	 * @return the field
	 */
	public long getField() {
		return field;
	}

	/**
	 * @param field the field to set
	 */
	public void setField(long field) {
		this.field = field;
	}
	
}
