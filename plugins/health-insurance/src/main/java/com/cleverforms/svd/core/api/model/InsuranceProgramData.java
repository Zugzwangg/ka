/*
 * Health insurance library (HI).
 * Copyright(c) 2010-2018.
 * December 27, 2013.
 * hi 
 * 
 */
package com.cleverforms.svd.core.api.model;

import java.io.Serializable;

import com.cleverforms.comms.server.converters.WithValueConverter;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;
/**
 * 
 * @author Sergey Titarchuk
 *
 */
@XStreamAlias("program")
@XStreamConverter(value = WithValueConverter.class, strings = "description")
public class InsuranceProgramData implements Serializable {

	/** The serial version UID */
	private static final long serialVersionUID = 5292825212825337366L;
	
	@XStreamAsAttribute
	private Long value;
	@XStreamAsAttribute 
	private String name;
	@XStreamAsAttribute 
	private double tariff;
	private String description;

	public InsuranceProgramData() {
	}
	
	public InsuranceProgramData(long value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the tariff
	 */
	public double getTariff() {
		return tariff;
	}

	/**
	 * @param tariff the tariff to set
	 */
	public void setTariff(double tariff) {
		this.tariff = tariff;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * @return the value
	 */
	public Long getValue() {
		return value;
	}
	
	/**
	 * @param value the value to set
	 */
	public void setValue(Long value) {
		this.value = value;
	}

}
