/*
 * Health insurance library (HI).
 * Copyright(c) 2010-2018.
 * December 25, 2013.
 * hi 
 * 
 */
package com.cleverforms.svd.core.api.model;

import java.util.Date;

import com.cleverforms.comms.server.model.BaseData;
import com.cleverforms.comms.server.util.Util;
import com.thoughtworks.xstream.annotations.XStreamAlias;
/**
 * 
 * @author Sergey Titarchuk
 *
 */
@XStreamAlias("human")
public class ConsumerData extends BaseData {

	/** The serial version UID */
	private static final long serialVersionUID = 4967372190572538326L;
	
	@XStreamAlias("last-name")
	private String lastName;
	@XStreamAlias("first-name")
	private String firstName;
	private Date birthday;
	private String phone;
	@XStreamAlias("e-mail")
	private String email;
	@XStreamAlias("zip-code")
	private String zipCode;
	private String address;
	
	public ConsumerData() {
	}
	
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the birthday
	 */
	public Date getBirthday() {
		return birthday;
	}

	/**
	 * @param birthday the birthday to set
	 */
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof ConsumerData) {
			ConsumerData data = (ConsumerData) obj;
			return Util.equalWithNull(lastName, data.getLastName()) &&	Util.equalWithNull(firstName, data.getFirstName()) && 
				Util.equalWithNull(birthday, data.getBirthday());
		}
		return super.equals(obj);
	}
	
	@Override
	public String toString() {
		return "ConsumerData[firstName = " + firstName +
			", lastName = " + lastName +
			", birthday = " + birthday +
			", phone = " + phone +
			", email = " + email +
			", zipCode = " + zipCode +
			", address = " + address +
			"]";
	}
	
}
