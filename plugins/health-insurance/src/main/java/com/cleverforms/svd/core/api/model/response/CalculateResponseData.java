/*
 * Health insurance library (HI).
 * Copyright(c) 2010-2018.
 * December 25, 2013.
 * hi 
 * 
 */
package com.cleverforms.svd.core.api.model.response;

import com.cleverforms.comms.server.model.BaseData;
import com.cleverforms.comms.server.model.ListWrapper;
import com.cleverforms.svd.core.api.model.AgeLimitList;
import com.cleverforms.svd.core.api.model.InsuranceProgramData;
import com.thoughtworks.xstream.annotations.XStreamAlias;
/**
 * 
 * @author Sergey Titarchuk
 *
 */
public class CalculateResponseData extends BaseData {

	/** The serial version UID*/
	private static final long serialVersionUID = -4648571846667346591L;
	
	private ListWrapper<InsuranceProgramData> programs;
	@XStreamAlias("age-limits")
	private AgeLimitList limits;

	public CalculateResponseData() {
		programs = new ListWrapper<InsuranceProgramData>();
		limits = new AgeLimitList();
	}

	/**
	 * @return the programs
	 */
	public ListWrapper<InsuranceProgramData> getPrograms() {
		return programs;
	}

	/**
	 * @param programs the programs to set
	 */
	public void setPrograms(ListWrapper<InsuranceProgramData> programs) {
		this.programs = programs;
	}

	/**
	 * @return the limits
	 */
	public AgeLimitList getLimits() {
		return limits;
	}

	/**
	 * @param limits the limits to set
	 */
	public void setLimits(AgeLimitList limits) {
		this.limits = limits;
	}
	
	@Override
	public String toString() {
		return "CalculateResponseData[programs = " + programs + ", limits = " + limits + "]";
	}

}
