/*
 * Health insurance library (HI).
 * Copyright(c) 2010-2018.
 * December 25, 2013.
 * hi 
 * 
 */
package com.cleverforms.svd.core.api.model.request;

import com.cleverforms.comms.server.model.ListWrapper;
import com.cleverforms.svd.core.api.model.ConsumerData;
import com.cleverforms.svd.core.api.model.CustomerData;
import com.thoughtworks.xstream.annotations.XStreamAlias;
/**
 * 
 * @author Sergey Titarchuk
 *
 */
public class SaveRequestData extends CalculateRequestData {

	/** The serial version UID */
	private static final long serialVersionUID = -219569313177383293L;
	
	private long program;
	private double price;
	@XStreamAlias("customer")	
	private CustomerData customer;
	private ListWrapper<ConsumerData> consumers;
	
	public SaveRequestData() {
	}
	
	/**
	 * @return the program
	 */
	public long getProgram() {
		return program;
	}

	/**
	 * @param program the program to set
	 */
	public void setProgram(long program) {
		this.program = program;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * @return the customer
	 */
	public CustomerData getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(CustomerData customer) {
		this.customer = customer;
	}

	/**
	 * @return the consumers
	 */
	public ListWrapper<ConsumerData> getConsumers() {
		return consumers;
	}

	/**
	 * @param consumers the consumers to set
	 */
	public void setConsumers(ListWrapper<ConsumerData> consumers) {
		this.consumers = consumers;
	}
	
	@Override
	public String toString() {
		return "SaveEntityRequestData[country = " + country
			+ ", amount = " + amount
			+ ", startDate = " + startDate
			+ ", endDate = " + endDate
			+ ", purposes = " + purposes
			+ ", program = " + program
			+ ", price = " + price
			+ ", customer = " + customer
			+ ", consumers = " + consumers
			+ "]";
	}
	
}
