/*
 * Health insurance library (HI).
 * Copyright(c) 2010-2018.
 * January 3, 2014.
 * hi 
 * 
 */
package com.cleverforms.svd.core.api.model;

import java.util.HashSet;

import com.cleverforms.comms.server.model.ListWrapper;

/**
 * 
 * @author Sergey Titarchuk
 *
 */
public class AgeLimitList extends ListWrapper<AgeLimit> {

	/** The serial version UID */
	private static final long serialVersionUID = 7551537313414525458L;

	public AgeLimitList() {
	}
	
	public Double ratio(Number age) {
		if(age != null) {
			for(AgeLimit limit : this) {
				if(limit.contains(age)) {
					return limit.getRatio();
				}
			}
		}
		return null;
	}
	
	@Override
	public String toString() {
		return "AgeLimitList" + new HashSet<AgeLimit>(this);
	}
}
