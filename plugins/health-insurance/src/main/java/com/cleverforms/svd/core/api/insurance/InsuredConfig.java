/*
 * Health insurance library (HI).
 * Copyright(c) 2010-2018.
 * January 3, 2014.
 * hi 
 * 
 */
package com.cleverforms.svd.core.api.insurance;

import com.cleverforms.svd.core.api.config.FieldConfig;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
/**
 * 
 * @author Sergey Titarchuk
 *
 */
public class InsuredConfig extends FieldConfig {

	/** The serial version UID */
	private static final long serialVersionUID = 1373357957433971026L;
	
	@XStreamAsAttribute
	@XStreamAlias("consumer-field")
	private long consumerField;  

	public InsuredConfig() {}
	
	public InsuredConfig(long field, long consumerField) {
		super(field);
		this.consumerField = consumerField;
	}

	/**
	 * @return the consumerField
	 */
	public long getConsumerField() {
		return consumerField;
	}

	/**
	 * @param consumerField the consumerField to set
	 */
	public void setConsumerField(long consumerField) {
		this.consumerField = consumerField;
	}

}
