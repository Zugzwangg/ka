/*
 * Health insurance library (HI).
 * Copyright(c) 2010-2018.
 * January 3, 2014.
 * hi 
 * 
 */
package com.cleverforms.svd.core.api.model.response;

import com.cleverforms.comms.server.model.BaseData;

/**
 * 
 * @author Sergey Titarchuk
 *
 */
public class SaveResponseData extends BaseData {

	/** The serial version UID */
	private static final long serialVersionUID = 8984535231762872576L;
	
	private long insurance;
	private long program;
	private double price;
	
	public SaveResponseData() {
	}

	public SaveResponseData(long insurance, long program, double price) {
		this.insurance = insurance;
		this.program = program;
		this.price = price;
	}
	
	/**
	 * @return the ID of insurance
	 */
	public long getInsurance() {
		return insurance;
	}
	
	/**
	 * @param insurance the ID of insurance to set
	 */
	public void setInsurance(long insurance) {
		this.insurance = insurance;
	}

	/**
	 * @return the program
	 */
	public long getProgram() {
		return program;
	}

	/**
	 * @param program the program to set
	 */
	public void setProgram(long program) {
		this.program = program;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		return "SignRequestData[insurance = " + insurance
			+ ", program = " + program
			+ ", price = " + price
			+ "]";
	}
	
}
