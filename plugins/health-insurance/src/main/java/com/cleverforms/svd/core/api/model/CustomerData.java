/*
 * Health insurance library (HI).
 * Copyright(c) 2010-2018.
 * October 13, 2014.
 * hi 
 * 
 */
package com.cleverforms.svd.core.api.model;

import com.cleverforms.comms.server.model.BaseData;
import com.cleverforms.comms.server.util.Util;
import com.cleverforms.ics.shared.dictionary.model.CustomerModel;
import com.cleverforms.ics.shared.dictionary.proxy.CustomerProxy;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 
 * @author Sergey Titarchuk
 *
 */
@XStreamAlias("customer")
public class CustomerData extends BaseData {

	/** The serial version UID */
	private static final long serialVersionUID = -1129207016999396615L;
	
	@XStreamAlias("last-name")
	private String lastName;
	@XStreamAlias("first-name")
	private String firstName;
	private String phone;
	@XStreamAlias("e-mail")
	private String email;
	
	public CustomerData() {
	}
	
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof CustomerData) {
			CustomerData data = (CustomerData) obj;
			return Util.equalWithNull(lastName, data.getLastName())
				&& Util.equalWithNull(firstName, data.getFirstName())
				&& Util.equalWithNull(email, data.getEmail());
		}
		return super.equals(obj);
	}
	
	@Override
	public String toString() {
		return "CustomerData[firstName = " + firstName +
			", lastName = " + lastName +
			", phone = " + phone +
			", email = " + email +
			"]";
	}
	
	public CustomerProxy asCustomerProxy() {
		CustomerProxy proxy = new CustomerModel();
		proxy.setName(firstName);
		proxy.setLastName(lastName);
		proxy.setEmail(email);
		proxy.setPhone(phone);
		return proxy;
	}
	
}
