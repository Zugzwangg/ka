/*
 * Health insurance library (HI).
 * Copyright(c) 2010-2018.
 * January 14, 2013.
 * hi 
 * 
 */
package com.cleverforms.svd.core.api.model.request;

import com.cleverforms.comms.server.model.BaseData;
/**
 * 
 * @author Sergey Titarchuk
 *
 */
public class InsuranceAmountsRequestData extends BaseData {

	/** The serial version UID */
	private static final long serialVersionUID = -8887913068019379315L;
	
	private long country;
	
	public InsuranceAmountsRequestData() {
	}

	public InsuranceAmountsRequestData(long country) {
		this.country = country;
	}

	/**
	 * @return the country
	 */
	public long getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(long country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "InsuranceAmountsRequestData[country = " + country + "]";
	}
	
}
