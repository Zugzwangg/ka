/*
 * Health insurance library (HI).
 * Copyright(c) 2010-2018.
 * December 25, 2013.
 * hi 
 * 
 */
package com.cleverforms.svd.core.api.model;

import com.cleverforms.comms.server.converters.WithValueConverter;
import com.cleverforms.comms.server.model.BaseData;
import com.cleverforms.comms.shared.marker.HasDescription;
import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;
/**
 * 
 * @author Sergey Titarchuk
 *
 */
@XStreamAlias("program")
@XStreamConverter(value = WithValueConverter.class, strings = "description")
public class InsuranceProgram extends BaseData implements HasDescription {

	/** The serial version UID */
	private static final long serialVersionUID = 261562127015475665L;

	/** Key to value property */
	public static final String VALUE = "value";

	private String description;
	@XStreamAsAttribute
	private Long value;

	public InsuranceProgram() {
	}
	
	public InsuranceProgram(long value, String description) {
		this.value = value;
		this.description = description;
	}
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * @return the value
	 */
	public Long getValue() {
		return value;
	}
	
	/**
	 * @param value the value to set
	 */
	public void setValue(Long value) {
		this.value = value;
	}

	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper().add(DESCRIPTION, description).add(VALUE, value);
	}
	
}
