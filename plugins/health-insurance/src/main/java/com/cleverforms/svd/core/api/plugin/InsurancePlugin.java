/*
 * Health insurance library (HI).
 * Copyright(c) 2010-2018.
 * October 13, 2015.
 * hi 
 * 
 */
package com.cleverforms.svd.core.api.plugin;

import static com.cleverforms.svd.core.api.insurance.InsurancePayload.SQL_AGE;
import static com.cleverforms.svd.core.api.insurance.InsurancePayload.SQL_AMOUNT;
import static com.cleverforms.svd.core.helper.PersistentHelper.populateDocument;
import static com.cleverforms.svd.core.helper.PersistentHelper.updateValueData;

import java.io.File;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.security.PublicKey;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DataBindingException;

import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.cleverforms.comms.server.model.BaseData;
import com.cleverforms.comms.server.model.ClientEntity;
import com.cleverforms.comms.server.util.Casting;
import com.cleverforms.comms.server.util.Util;
import com.cleverforms.comms.shared.annotation.RESTMethod;
import com.cleverforms.comms.shared.exception.APIException;
import com.cleverforms.comms.shared.exception.DBException;
import com.cleverforms.comms.shared.exception.DataResolverException;
import com.cleverforms.comms.shared.exception.ErrorType;
import com.cleverforms.comms.shared.exception.ForbiddenException;
import com.cleverforms.comms.shared.marker.Processing;
import com.cleverforms.comms.shared.model.NamedModel;
import com.cleverforms.comms.shared.proxy.BaseProxy;
import com.cleverforms.comms.shared.util.Format;
import com.cleverforms.comms.shared.util.ManyOfManyFormat;
import com.cleverforms.comms.shared.util.date.DateTime;
import com.cleverforms.ics.core.api.TransactionType;
import com.cleverforms.ics.core.api.config.RequestDataPacketImpl;
import com.cleverforms.ics.core.api.plugin.ICSAPIPluginImpl;
import com.cleverforms.ics.db.model.dictionary.Consumer;
import com.cleverforms.ics.db.model.dictionary.Customer;
import com.cleverforms.ics.db.model.orgchart.Person;
import com.cleverforms.ics.db.model.svd.Document;
import com.cleverforms.ics.db.model.svd.Field;
import com.cleverforms.ics.db.model.svd.Form;
import com.cleverforms.ics.db.model.svd.Value;
import com.cleverforms.ics.shared.DictionaryType;
import com.cleverforms.ics.shared.dictionary.proxy.CustomerProxy;
import com.cleverforms.ics.shared.pub.proxy.DigitalSignatureProxy;
import com.cleverforms.ics.shared.svd.enumerator.FieldPropertyType;
import com.cleverforms.svd.core.api.config.FormConfig;
import com.cleverforms.svd.core.api.config.FormsConfig;
import com.cleverforms.svd.core.api.insurance.InsurancePayload;
import com.cleverforms.svd.core.api.model.AgeLimit;
import com.cleverforms.svd.core.api.model.AgeLimitList;
import com.cleverforms.svd.core.api.model.ConsumerData;
import com.cleverforms.svd.core.api.model.CustomerData;
import com.cleverforms.svd.core.api.model.InsuranceProgram;
import com.cleverforms.svd.core.api.model.InsuranceProgramData;
import com.cleverforms.svd.core.api.model.request.CalculateRequestData;
import com.cleverforms.svd.core.api.model.request.InsuranceAmountsRequestData;
import com.cleverforms.svd.core.api.model.request.SaveRequestData;
import com.cleverforms.svd.core.api.model.response.CalculateResponseData;
import com.cleverforms.svd.core.api.model.response.SaveResponseData;
import com.cleverforms.svd.core.calculator.DocumentCalculatorFactory;
import com.cleverforms.svd.core.calculator.DocumentDataResolver;
import com.cleverforms.svd.core.helper.PersistentHelper;

/**
 * A {@code ICSAPIPlugin} for work with health insurance.
 * @author Sergey Titarchuk
 */
@Transactional
@RESTMethod("insurance")
public class InsurancePlugin extends ICSAPIPluginImpl {

	private InsurancePayload config = new InsurancePayload();
	
	public InsurancePlugin() {}

	public void setFormsConfig(File fileConfig) {
		FormsConfig formsConfig = FormsConfig.load(fileConfig, InsurancePayload.class);
		for (FormConfig formConfig : formsConfig.getForms()) {
			if (formConfig instanceof InsurancePayload) {
				config = (InsurancePayload) formConfig;
				break;
			}
		}
	}
	
	protected Customer populateCustomer(CustomerData customerData) throws DBException {
		final String userName = Util.isNotBlank(customerData.getPhone()) ? customerData.getPhone() : customerData.getEmail();
		Customer customer = Util.isNotBlank(userName) ? Customer.getByUserName(provider(), userName) : null;
		if (customer == null) {
			CustomerProxy proxy = customerData.asCustomerProxy();
			proxy.setPassword(Util.generatePassword(6, 12));
			try {
				customer = provider().addCustomer(proxy);
			} catch (Exception e) {
				throw new DataBindingException("Can't add customer", e);
			}
		}
		return customer;
	}
	
	protected Consumer populateConsumer(ConsumerData consumerData, String locale) throws DBException {
		consumerData.setBirthday(DateTime.clearTime(consumerData.getBirthday()));
		Consumer consumer = provider().persistentUtil().findConsumer(consumerData.getFirstName(), consumerData.getLastName(), consumerData.getBirthday());
		if (consumer == null){
			consumer = new Consumer();
			consumer.setName(Format.get().toTranslit(consumerData.getFirstName()));
			consumer.setLastName(Format.get().toTranslit(consumerData.getLastName()));
			consumer.setBirthday(consumerData.getBirthday());
		}
		consumer.setEmail(consumerData.getEmail());
		consumer.setPhone(consumerData.getPhone());
		consumer.setZipCode(consumerData.getZipCode());
		consumer.setAddress(consumerData.getAddress());
		return consumer;
	}

 	protected Map<Long, Value> populateInsurance(Document insurance, ClientEntity author, Document country, Document amount,
 			Customer customer, long program, CalculateRequestData requestData) throws DBException {
 		// set data to document
		Map<Long, Value> values = populateDocument(insurance, config.getForm(), author, config.getSector());
		if(country != null)
			updateValueData(values.get(config.getCountry().getField()), String.valueOf(country.getId()));
		if(amount != null)
			updateValueData(values.get(config.getAmount().getField()), String.valueOf(amount.getId()));
		if(customer != null)
			updateValueData(values.get(config.getInsurer().getField()), String.valueOf(customer.getId()));
		updateValueData(values.get(config.getPurposes().getField()), String.valueOf(requestData.getPurposes()));
		updateValueData(values.get(config.getStart().getField()), requestData.getStartDate());
		updateValueData(values.get(config.getEnd().getField()), requestData.getEndDate());
		updateValueData(values.get(config.getPrograms().getField()), String.valueOf(program));
		return values;
	}
 	
 	protected double calculateTariff(Document insurance, ClientEntity author, Document country, Document amount,
 			Customer customer, long program, CalculateRequestData requestData) throws DBException {
 		// set data to document
		Map<Long, Value> values = populateInsurance(insurance, author, country, amount, customer, program, requestData);
		// calculate insurance
		insurance = DocumentCalculatorFactory.get().calculate(insurance);
		// get value of tariff and round his
		Value tariffValue = values.get(config.getTariff().getField());
		tariffValue = updateValueData(tariffValue, String.valueOf(Util.round((Double) Casting.asDouble(tariffValue.getData()), 2))); 
		return Casting.asDouble(tariffValue.getData());
 	}
 	
 	protected DocumentDataResolver saveInsurance(Document insurance, Map<Long, Value> values) throws DBException {
		// calculate insurance
		DocumentDataResolver resolver = DocumentCalculatorFactory.get().calculateProxy(insurance, null);
		// get value of tariff and round his
		Value tariffValue = values.get(config.getTariff().getField());
		resolver.getData().set(String.valueOf(config.getTariff().getField()), Casting.asDouble(
			updateValueData(tariffValue, String.valueOf(Util.round((Double) Casting.asDouble(tariffValue.getData()), 2))).getData()));
 		insurance = controller().saveDocument(insurance);
 		return resolver;
 	}
 	
	protected Document calculateInsured(Form form, ClientEntity author, Document parent, BaseProxy parentData, Consumer consumer, long consumerField) throws DBException {
		Document insured = new Document();
		Map<Long, Value> values = populateDocument(insured, form, author, parent, null);
		debug(updateValueData(values.get(consumerField), String.valueOf(consumer.getId())));
		return DocumentCalculatorFactory.get().calculate(insured, parentData);
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	protected CalculateResponseData getCalculateResponse(ClientEntity author, Document country, Document amount,
			Map<Long, String> programs, String programsName, AgeLimitList limits, CalculateRequestData requestData) throws DBException {
		CalculateResponseData responseData = new CalculateResponseData();
		responseData.setLimits(limits);
		for(InsuranceProgram program : config.getPrograms()) {
			if(programs.containsKey(program.getValue())) {
				final long programNumber = program.getValue();
				InsuranceProgramData programData = new InsuranceProgramData(programNumber, program.getDescription());
				programData.setName(programsName + " " + programs.get(programNumber));
				programData.setTariff(calculateTariff(new Document(), author, country, amount, null, programNumber, requestData));
				responseData.getPrograms().add(programData);
			}
		}
		return responseData;
	}

	protected SaveResponseData getSaveResponse(ClientEntity author, Document country, Document amount,
			AgeLimitList limits, SaveRequestData requestData, String locale) throws DBException {
		Form formTable = provider().findReference(provider().find(Field.class, config.getConsumers().getField()));
		debug(formTable);
		if (formTable == null) 
			throw new DataResolverException(ErrorType.ILLEGAL_ARGUMENTS, "Invalid insured's table form!");
		// save insurer data
		Customer customer = populateCustomer(requestData.getCustomer());
		// create, calculate and save insurance document
		Document insurance = new Document(); 
 		// set data to document
		Map<Long, Value> values = populateInsurance(insurance, author, country, amount, customer, requestData.getProgram(), requestData);
		BaseProxy insuranceData = saveInsurance(insurance, values).getData();
		// sets insurance price, tariff and duration
		double price = 0, tariff = Casting.asDouble(insuranceData.get(String.valueOf(config.getTariff().getField())));
		debug(tariff);
		// add table data
		for (ConsumerData consumerData : requestData.getConsumers()) {
			// check age limits
			Double ratio = limits.ratio(DateTime.diff(new Date(), consumerData.getBirthday(), "year"));
			if (ratio == null) {
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				throw new ForbiddenException(ErrorType.ILLEGAL_ARGUMENTS, "The age limit for insurance consumer: " + consumerData);
			}
			// calculate insurance price
			price = Util.round(price + tariff * ratio, 2);
			// save insured consumer in dictionary
			Consumer consumer = provider().save(populateConsumer(consumerData, locale));
			// add row in insured table
			controller().saveDocument(calculateInsured(formTable, author, insurance, insuranceData, consumer, config.getConsumers().getConsumerField()));
		}
		// check insurance price
		if (Util.round(requestData.getPrice(), 2) != price){
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			throw new APIException(ErrorType.ILLEGAL_ARGUMENTS, "Invalid insurance price: expected = " + requestData.getPrice() + ", calculated = " + price);
		}
		// save summary price 
		Value valuePrice = values.get(config.getPrice().getField());
		if (valuePrice != null) {
			valuePrice.setData(String.valueOf(price));
		}
		return new SaveResponseData(insurance.getId(), requestData.getProgram(), requestData.getPrice());
	}
	
	@SuppressWarnings("unchecked")
	protected <R extends CalculateRequestData> BaseData createInsurance(Class<R> type, ClientEntity author, Reader reader,
			MediaType mediaType, PublicKey publicKey, String locale) throws Exception {
		Field programsField = provider().find(Field.class, config.getPrograms().getField());
		if (programsField == null) 
			throw new DataResolverException(ErrorType.ILLEGAL_ARGUMENTS, "Invalid insurance program field!");
		Map<Long, String> programs = ManyOfManyFormat.getItemsMap(programsField.getProperty(FieldPropertyType.MASK));
		
		if (programs.isEmpty())
			throw new DataResolverException(ErrorType.ILLEGAL_ARGUMENTS, "Invalid insurance program field configuration!");
		RequestDataPacketImpl dp = readRequest(type, reader, mediaType, publicKey);
		TransactionType transType = null;
		if (type == CalculateRequestData.class) {
			transType = TransactionType.CALCULATE;
		} else if (type == SaveRequestData.class) {
			transType = TransactionType.SAVE;
		}

		// check transaction 
		checkRequest(dp, transType);
		R requestData = (R) dp.getData();
		Document country = provider().find(Document.class, requestData.getCountry());
		if (country == null)
			throw new DataResolverException(ErrorType.ILLEGAL_ARGUMENTS, "Invalid document's country!");
		Document amount =  provider().find(Document.class, requestData.getAmount());
		if (amount == null)
			throw new DataResolverException(ErrorType.ILLEGAL_ARGUMENTS, "Invalid document's amount!");
		List<Object[]> ages = provider().getNativeResultList(config.getQuery(SQL_AGE), null, 0, -1, country.getId());
		if (ages == null || ages.isEmpty())
			throw new DBException(ErrorType.ERROR_READ_DATA, "Invalid age restriction!");
		AgeLimitList limits = new AgeLimitList();
		for (Object[] age : ages) {
			byte from = new Byte(((String) age[0]).substring(((String) age[0]).length() - 2)).byteValue();
			byte to = new Byte(((String) age[1]).substring(((String) age[0]).length() - 2)).byteValue();
			limits.add(new AgeLimit(from, to, ((Number) age[2]).doubleValue()));
		}
		switch (transType) {
		case CALCULATE:
			return getCalculateResponse(author, country, amount, programs, programsField.getName(), limits, requestData);
		case SAVE:
			return getSaveResponse(author, country, amount, limits, (SaveRequestData) requestData, locale);
		default:
			throw new APIException(ErrorType.ILLEGAL_ARGUMENTS, "Invalid request data or transaction type!");
		}
	}

	protected DigitalSignatureProxy signInsurance(final SaveResponseData requestData, ClientEntity user) {
		final Document insurance = provider().find(Document.class, requestData.getInsurance());
		return controller().signDocument(insurance, config.getForm(), user, new Processing<Document>() {
			@Override
			public void onProcess(Document object) {
				// check price and program
				double price = Double.MIN_VALUE; long program = Long.MIN_VALUE;
				for (Value value : insurance.getValues()) {
					final long fid = value.getField().getId();
					if (fid == config.getPrice().getField()) {
						price = Casting.asDouble(value.getData());
					} else if(fid == config.getPrograms().getField()) {
						program = Casting.asLong(value.getData());
					}
				}
				if (requestData.getPrice() != price || requestData.getProgram() != program)
					throw new DataResolverException(ErrorType.ILLEGAL_ARGUMENTS, "Invalid document's price or program!");
			}
		});
	}

	@RESTMethod("countries")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public void loadCountries(HttpServletRequest request, HttpServletResponse response, PublicKey publicKey,
			ClientEntity user, Reader reader, Writer writer, MediaType mediaType) throws Exception {
		RequestDataPacketImpl dp = readRequest(reader, mediaType, publicKey);
		// check request transaction 
		checkRequest(dp, TransactionType.DICTIONARY_LIST);
		writeResponse((Serializable) PersistentHelper.
			loadDictionary(config.getCountry().getField(),
				user instanceof Person 
					? ((Person) user).getUnit() 
					: (user instanceof Customer ? ((Customer) user).getUnit() : null), 
				controller().supportedLanguage(request)), writer, mediaType);
	}
	
	@RESTMethod("amounts")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public void loadInsuranceAmounts(HttpServletRequest request, HttpServletResponse response, PublicKey publicKey, 
			ClientEntity user, Reader reader, Writer writer, MediaType mediaType) throws Exception {
		RequestDataPacketImpl dp = readRequest(InsuranceAmountsRequestData.class, reader, mediaType, publicKey);
		// check request transaction 
		checkRequest(dp, TransactionType.DICTIONARY_LIST);
		writeResponse((Serializable) PersistentHelper.
			loadNativeDictionary(config.getQuery(SQL_AMOUNT), false, 
				((InsuranceAmountsRequestData) dp.getData()).getCountry()), NamedModel.class, "amount", writer, mediaType);
	}

	@RESTMethod("new")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public void newRecord(HttpServletRequest request, HttpServletResponse response, PublicKey publicKey,
			ClientEntity user, Reader reader, Writer writer, MediaType mediaType) throws Exception {
		RequestDataPacketImpl dp = readRequest(reader, mediaType, publicKey);
		// check request transaction 
		checkRequest(dp, TransactionType.NEW);
		Document document = controller().newDocumentInstance(config.getForm(), config.getSector(), user);
		controller().writeDocument(document, config.getForm(), DictionaryType.DOCUMENT, "uk", writer, mediaType);
	}

	@RESTMethod("calculate")
	public void calculateInsurance(HttpServletRequest request, HttpServletResponse response, PublicKey publicKey, 
			ClientEntity user, Reader reader, Writer writer, MediaType mediaType) throws Exception {
		writeResponse(createInsurance(CalculateRequestData.class, user, reader, mediaType, publicKey, null), writer, mediaType);
	}
	
	@RESTMethod("save")
	@Transactional(readOnly = false, rollbackFor = { APIException.class, DBException.class, DataResolverException.class, ForbiddenException.class })
	public void save(HttpServletRequest request, HttpServletResponse response, PublicKey publicKey, 
			ClientEntity user, Reader reader, Writer writer, MediaType mediaType) throws Exception {
		writeResponse(createInsurance(SaveRequestData.class, user, reader, mediaType, publicKey, controller().supportedLanguage(request)), writer, mediaType);
	}

	@RESTMethod("sign")
	@Transactional(readOnly = false, rollbackFor={ DBException.class, DataResolverException.class, ForbiddenException.class })
	public void signInsurance(HttpServletRequest request, HttpServletResponse response, PublicKey publicKey, 
			ClientEntity user, Reader reader, Writer writer, MediaType mediaType) throws Exception {
		RequestDataPacketImpl dp = readRequest(SaveResponseData.class, reader, mediaType, publicKey);
		// check request transaction 
		checkRequest(dp, TransactionType.SIGN);
		writeResponse(signInsurance((SaveResponseData) dp.getData(), user), writer, mediaType);
	}
	
}
