/*
 * Health insurance library (HI).
 * Copyright(c) 2010-2018.
 * December 30, 2013.
 * hi 
 * 
 */
package com.cleverforms.svd.core.api.model;

import com.cleverforms.comms.server.model.BaseData;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
/**
 * 
 * @author Sergey Titarchuk
 *
 */
@XStreamAlias("age-limit")
public class AgeLimit extends BaseData {

	/** The serial version UID */
	private static final long serialVersionUID = 3565632252349349925L;
	
	@XStreamAsAttribute
	private byte from;
	@XStreamAsAttribute
	private byte to;
	@XStreamAsAttribute
	private double ratio;
	
	public AgeLimit() {
	}
	
	public AgeLimit(byte from, byte to, double ratio) {
		this.from = from;
		this.to = to;
		this.ratio = ratio;
	}
	
	/**
	 * @return the from
	 */
	public byte getFrom() {
		return from;
	}
	
	/**
	 * @param from the from to set
	 */
	public void setFrom(byte from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public byte getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(byte to) {
		this.to = to;
	}

	/**
	 * @return the ratio
	 */
	public double getRatio() {
		return ratio;
	}

	/**
	 * @param ratio the ratio to set
	 */
	public void setRatio(double ratio) {
		this.ratio = ratio;
	}

	public boolean contains(Number age) {
		return age != null && from <= age.byteValue() && to >= age.byteValue();
	}
	
	@Override
	public String toString() {
		return "AgeLimit[from = " + from + ", to = " + to + ", ratio = " + ratio + "]";
	}
	
}
