/*
 * Health insurance library (HI).
 * Copyright(c) 2010-2018.
 * December 20, 2013.
 * hi 
 * 
 */
package com.cleverforms.svd.core.api;

import java.util.Date;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cleverforms.comms.server.RESTAPITestHelper;
import com.cleverforms.comms.server.model.BaseData;
import com.cleverforms.comms.server.model.ListWrapper;
import com.cleverforms.comms.shared.util.Util;
import com.cleverforms.comms.shared.util.date.DateTime;
import com.cleverforms.comms.shared.util.date.DateTime.Unit;
import com.cleverforms.ics.core.ICSRESTControllerTest;
import com.cleverforms.ics.core.api.TransactionType;
import com.cleverforms.ics.core.converters.ICSXStreamHelper;
import com.cleverforms.svd.core.api.model.ConsumerData;
import com.cleverforms.svd.core.api.model.CustomerData;
import com.cleverforms.svd.core.api.model.request.CalculateRequestData;
import com.cleverforms.svd.core.api.model.request.InsuranceAmountsRequestData;
import com.cleverforms.svd.core.api.model.request.SaveRequestData;
import com.cleverforms.svd.core.api.model.response.SaveResponseData;
import com.cleverforms.svd.core.provider.PersistentProvider;

/** 
 * Test of Controller for API operations.
 * @author Sergey Titarchuk
 */
@Rollback(true)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/application-context.xml"})
@RequestMapping("/api")
public class RESTControllerTest extends ICSRESTControllerTest {

	@Override
	public void initialize() {
		super.initialize();
		ICSXStreamHelper.COMPACT_MODE = false;
	}
	
	@Override
	protected PersistentProvider provider() {
		return (PersistentProvider) super.provider();
	}
	
	public BaseData calculateRequestData(long country, long amount, long purposes) {
    	CalculateRequestData data = new CalculateRequestData();
    	data.setAmount(amount);
    	data.setCountry(country);
    	data.setStartDate(DateTime.add(new Date(), 2, Unit.DAY));
    	data.setEndDate(DateTime.add(new Date(), 20, Unit.DAY));
    	data.setPurposes(purposes);
    	return data;
	}
	
	public SaveRequestData saveRequestData(long country, long amount, long purposes, 
			long program, double tariff, RESTAPITestHelper helper) throws Exception {
		SaveRequestData data = new SaveRequestData();
    	data.setAmount(amount);
    	data.setCountry(country);
    	data.setStartDate(new DateTime().addDays(2).clearTime().asDate());
    	data.setEndDate(new DateTime().addDays(20).clearTime().asDate());
    	data.setPurposes(purposes);
    	data.setProgram(program);
    	data.setCustomer(customerData(helper, "titarchuk.xml"));
    	data.setConsumers(insured(helper, "titarchuk.xml", "butov.xml"));
    	data.setPrice(Util.round(tariff * data.getConsumers().size(), 2));
		return data;
	}
	
	public CustomerData customerData(RESTAPITestHelper helper, String path) throws Exception {
		ICSXStreamHelper xHelper = new ICSXStreamHelper(helper.mediaType());
		xHelper.alias("human", CustomerData.class);
		xHelper.ignoreUnknownElements();
		return xHelper.read(CustomerData.class, helper.reader(path));
	}

	public ConsumerData humanData(RESTAPITestHelper helper, String path) throws Exception {
		ICSXStreamHelper xHelper = new ICSXStreamHelper(helper.mediaType());
		return xHelper.read(ConsumerData.class, helper.reader(path));
	}
	
	public ListWrapper<ConsumerData> insured(RESTAPITestHelper helper, String... paths) throws Exception {
		ListWrapper<ConsumerData> data = new ListWrapper<ConsumerData>();
		for(String path : paths) {
			data.add(humanData(helper, path));
		}
		return data;
	}
	
	@Test
    public void testCountries() throws Exception {
//    	Map<String, String> params = clientParams(person, "document.insurance.countries");
    	Map<String, String> params = serverParams(person, "document.insurance.countries");
    	params.put("locale", "ru");
    	// for JSON
    	debug(request(params, requestAsString(TransactionType.DICTIONARY_LIST)));
    	// for XML
    	helper.setMediaType(MediaType.APPLICATION_XML);
    	debug(request(params, requestAsString(TransactionType.DICTIONARY_LIST)));
    }

	@Test
    public void testAmounts() throws Exception {
//    	Map<String, String> params = clientParams(person, "document.insurance.amounts");
    	Map<String, String> params = serverParams(person, "document.insurance.amounts");
    	// for JSON
    	debug(request(params, requestAsString(TransactionType.DICTIONARY_LIST, new InsuranceAmountsRequestData(39))));
    	// for XML
    	helper.setMediaType(MediaType.APPLICATION_XML);
    	debug(request(params, requestAsString(TransactionType.DICTIONARY_LIST, new InsuranceAmountsRequestData(39))));
    }

	@Test
    public void testNewInsurance() throws Exception {
//    	Map<String, String> params = clientParams(person, "document.insurance.new");
    	Map<String, String> params = serverParams(person, "document.insurance.new");
    	// for JSON
    	debug(request(params, requestAsString(TransactionType.NEW)));
    	// for XML
    	helper.setMediaType(MediaType.APPLICATION_XML);
    	debug(request(params, requestAsString(TransactionType.NEW)));
    }
	
	@Test
	public void testCalculateInsurance() throws Exception {
//    	Map<String, String> params = clientParams(person, "document.insurance.calculate");
    	Map<String, String> params = serverParams(person, "document.insurance.calculate");
    	// for JSON
//    	debug(request(params, requestAsString(TransactionType.CALCULATE, calculateRequestData(39, 3, 0))));
    	// for XML
    	helper.setMediaType(MediaType.APPLICATION_XML);
    	debug(request(params, requestAsString(TransactionType.CALCULATE, calculateRequestData(39, 3, 0))));
	}

	@Test
	public void testSave() throws Exception {
//    	Map<String, String> params = clientParams(person, "document.insurance.save");
    	Map<String, String> params = serverParams(person, "document.insurance.save");
    	params.put("locale", "en");
    	// for JSON
//    	debug(request(params, requestAsString(TransactionType.SAVE, saveRequestData(39, 3, 0, 1, 18.9, helper))));
    	// for XML
    	helper.setMediaType(MediaType.APPLICATION_XML);
    	debug(requestAsString(TransactionType.SAVE, saveRequestData(39, 3, 0, 1, 18.9, helper)));
//    	debug(request(params, requestAsString(TransactionType.SAVE, saveRequestData(39, 3, 0, 1, 18.9, helper))));
	}
	
	@Test
	public void testSign() throws Exception {
//    	Map<String, String> params = clientParams(person, "document.insurance.save");
    	Map<String, String> params = serverParams(person, "document.insurance.sign");
    	// for JSON
//    	debug(request(params, requestAsString(TransactionType.SIGN, new SaveResponseData(155, 1, 37.8))));
    	// for XML
    	helper.setMediaType(MediaType.APPLICATION_XML);
    	params.put("uid", "88");
    	debug(request(params, requestAsString(TransactionType.SIGN, new SaveResponseData(152, 1, 37.8))));
	}
	
}