/*
 * Health insurance library (HI).
 * Copyright(c) 2010-2018.
 * December 20, 2013.
 * hi 
 * 
 */
package com.cleverforms.svd.core.api;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cleverforms.ics.core.ICSRESTAPITestHelper;
import com.cleverforms.ics.core.api.TransactionType;
import com.cleverforms.svd.core.api.model.request.InsuranceAmountsRequestData;
import com.cleverforms.svd.core.api.model.response.SaveResponseData;

/** 
 * Tests API operations.
 * @author Sergey Titarchuk
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/application-context.xml"})
public class HIAPITest {
	
	public static class TestHelper extends ICSRESTAPITestHelper {

		public TestHelper(Map<String, ?> configuration) {
			super(HIAPITest.class, configuration);
		}
		
	}

	protected TestHelper helper;
	protected RESTControllerTest controllerTest; 
	
	@Before
	public void initialize() throws Exception {
		Map<String, Object> configuration = new HashMap<String, Object>();
		configuration.put("appId", 1l);
		configuration.put("userId", 1l);
//		configuration.put("userId", 21l); // Носко
		configuration.put("baseUrl", "http://195.250.43.69:8080/svd/handler");
//		configuration.put("baseUrl", "http://192.168.93.240:8080/svd/handler");
//		configuration.put("baseUrl", "https://clever-forms.com:8443/svddemo/handler");
		configuration.put("requestMethod", RequestMethod.POST);
//		configuration.put("hashAlgorithm", HashAlgorithm.SHA);
//		configuration.put("mediaType", MediaType.APPLICATION_JSON);
		configuration.put("mediaType", MediaType.APPLICATION_XML);
		configuration.put("hostname", "kiyavia.com");
		helper = new TestHelper(configuration);
		controllerTest = new RESTControllerTest();
	}
	
	protected String requestAsString(TransactionType type, Serializable data) {
		return helper.requestAsString(type, data);
	}
	
	protected String requestAsString(TransactionType type) {
		return requestAsString(type, null);
	}

	protected void testRequest(String handlerUri, String method, TransactionType type, Serializable data) {
		helper.testRequest(handlerUri, method, helper.requestAsString(type, data));
	}
	
	protected void testRequest(String handlerUri, String method, TransactionType type) {
		testRequest(handlerUri, method, type, null);
	}

	protected void testRequest(String handlerUri, String method) {
		helper.testRequest(handlerUri, method);
	}
	
	@Test
	public void testParameters() throws MalformedURLException {
		helper.debug(helper.publicKey());
		helper.debug(helper.privateKey());
		
		String uri = helper.url("/hi");
		helper.debug("url: " + helper.getURL(uri, helper.publicKey(), helper.baseParams("insurance.countries")));
		helper.debug("url: " + helper.getURL(uri, helper.publicKey(), helper.baseParams("insurance.amounts")));
		helper.debug("url: " + helper.getURL(uri, helper.publicKey(), helper.baseParams("insurance.new")));
		helper.debug("url: " + helper.getURL(uri, helper.publicKey(), helper.baseParams("insurance.calculate")));
		helper.debug("url: " + helper.getURL(uri, helper.publicKey(), helper.baseParams("insurance.save")));
		helper.debug("url: " + helper.getURL(uri, helper.publicKey(), helper.baseParams("insurance.sign")));
	}
	
	@Test
	public void getRequestData() throws Exception {
		helper.debug("data: " + requestAsString(TransactionType.DICTIONARY_LIST));
		helper.debug("data: " + requestAsString(TransactionType.DICTIONARY_LIST, new InsuranceAmountsRequestData(69)));
		helper.debug("data: " + requestAsString(TransactionType.NEW));
		helper.debug("data: " + requestAsString(TransactionType.CALCULATE, controllerTest.calculateRequestData(69, 8, 1)));
		helper.debug("data: " + requestAsString(TransactionType.SAVE, controllerTest.saveRequestData(69, 8, 1, 1, 41.58, helper)));
		helper.debug("data: " + requestAsString(TransactionType.SIGN, new SaveResponseData(354, 1, 1496.88)));
	}
	
	@Test
	public void testGetCertificate() {
		helper.testRequest("/api/certificate/kiyavia.com.cer?app_id=1&serial_number=1412858472296", "GET");
	}
	
	@Test
	public void testGetKeyStore() {
		helper.testRequest("/api/keystore/kiyavia.com.keystore?app_id=1&serial_number=1412858472296&password=12345", "GET");
	}
	
	@Test
	public void testRequest() throws Exception {
		testRequest("/api", "document.insurance.countries", TransactionType.DICTIONARY_LIST);
//		testRequest("/hi", "insurance.amounts", TransactionType.DICTIONARY_LIST, new InsuranceAmountsRequestData(71));
//		testRequest("/hi", "insurance.new", TransactionType.NEW);
//		testRequest("/hi", "insurance.calculate", TransactionType.CALCULATE, controllerTest.calculateRequestData(71, 3, 1));
//		testRequest("/hi", "insurance.save", TransactionType.SAVE, controllerTest.saveRequestData(71, 3, 1, 1, 37.8, helper));
//		testRequest("/hi", "insurance.sign", TransactionType.SIGN, new SaveResponseData(314, 1, 75.6));
	}

}
